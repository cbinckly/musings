from django.apps import AppConfig


class MusingConfig(AppConfig):
    name = 'musing'
