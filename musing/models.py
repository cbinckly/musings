import random
import string

from django.db import models

from musing.data import BAD_WORDS


class Tag(models.Model):

    tag = models.CharField(max_length=32, unique=True)

    @classmethod
    def extract_from(cls, content):
        _tags = {i for i in content.lower().split() if i.startswith("#")}
        print("Found tags {}".format(_tags))
        tags = []
        for t in _tags:
            try:
                tags.append(Tag.objects.get(tag=t))
            except:
                pass
        return tags

    def __str__(self):
        return self.tag


class Question(models.Model):

    class Meta:
        ordering = ('order', )

    content = models.CharField(max_length=1600)
    tags = models.ManyToManyField(Tag, related_name="questions")
    order = models.PositiveIntegerField(default=100)

    def musings(self):
        return Musing.objects.filter(tags__in=self.tags.all())

    def approved_musings(self):
        return Musing.objects.filter(tags__in=self.tags.all(), approved=True)

    def next_musing(self):
        undisplayed = self.approved_musings().filter(displayed=False)
        if undisplayed:
            return undisplayed.first()

        musings = self.approved_musings.count()
        if not musings:
            return Musing(content="There are no musings for this question.")

        selected = random.randrange(
                self.approved_musings.count())

        return self.approved_musings.all()[selected]

    def __str__(self):
        return self.content


class ApprovedMusingsManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(approved=True)

    def for_question(self, question):
        return self.get_queryset().filter(tags__in=question.tags.all())

    def for_tag(self, hashtag):
        return self.get_queryset().filter(tags__in=[hashtag])

    def undisplayed(self):
        return self.get_queryset().filter(
                displayed=False).order_by('received_on')

    def next(self):
        if self.undisplayed():
            return self.undisplayed().first()
        return self.get_queryset().all()[
                random.randrange(self.get_queryset().count())]

    def undisplayed_for_tag(self, hashtag):
        return self.for_tag(hashtag).filter(
                displayed=False).order_by('received_on')

    def next_for_tag(self, hashtag):
        undisplayed = self.undisplayed_for_tag(hashtag).first()
        if undisplayed:
            return undisplayed.first()

        count = self.for_tag(hashtag).count()
        if not count:
            return None

        return self.for_tag(hashtag).all()[random.randrange(count)]

    def undisplayed_for_question(self, question):
        return self.for_question(question).filter(
                displayed=False).order_by('received_on')

    def next_for_question(self, question):
        undisplayed = self.undisplayed_for_question(question)
        if undisplayed:
            return undisplayed.first()

        count = self.for_question(question).count()
        if not count:
            return None

        return self.for_question(question).all()[random.randrange(count)]


class Musing(models.Model):

    areacode = models.CharField(max_length=7)
    content = models.CharField(max_length=1600)

    tags = models.ManyToManyField(Tag, related_name="musings")

    received_on = models.DateTimeField(auto_now_add=True)

    displayed = models.BooleanField(default=False)
    approved = models.BooleanField(default=False)

    objects = models.Manager()
    approved_objects = ApprovedMusingsManager()

    def assess(self):
        cleaned = self.content.translate(
                str.maketrans('', '', string.punctuation))
        words = { w.lower() for w in cleaned.split() }
        if not words & BAD_WORDS:
            self.approved = True
        self.save()
        return self.approved

    def display(self):
        self.displayed = True
        return self.save()

    def __str__(self):
        content = self.content
        if len(content) > 60:
            content = self.content[:57] + '...'
        return content
