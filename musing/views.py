import random

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from rest_framework import status
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response

from twilio.twiml.messaging_response import MessagingResponse

from musing.models import Question, Tag, Musing
from musing.serializers import MusingContentSerializer, QuestionSerializer

class RetrieveRandomMusing(APIView):

    def get(self, request, format=None):
        musing = Musing.approved_objects.next()
        if musing:
            musing.display()
            return Response(MusingContentSerializer(instance=musing).data)
        return Response("", status.HTTP_404_NOT_FOUND)

class RetrieveNextMusingForQuestion(APIView):

    def get(self, request, question_id=None, format=None):
        if not question_id:
            return Response("", status.HTTP_400_BAD_REQUEST)

        question = get_object_or_404(Question, pk=question_id)
        musing = Musing.approved_objects.next_for_question(question)
        if musing:
            musing.display()
            return Response(MusingContentSerializer(instance=musing).data)
        return Response("", status.HTTP_404_NOT_FOUND)

class QuestionList(generics.ListAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

class RetrieveNextMusingForTag(APIView):

    def get(self, request, hashtag=None, format=None):
        if not hashtag:
            return Response("", status.HTTP_400_BAD_REQUEST)

        hashtag = "#{}".format(hashtag)
        tag = get_object_or_404(Tag, tag=hashtag)
        musing = Musing.approved_objects.next_for_tag(tag)
        musing.display()
        return Response(MusingContentSerializer(instance=musing).data)

class TwilioSMSReceiveEndpoint(APIView):

    @csrf_exempt
    def post(self, request, format=None):
        response = MessagingResponse()
        from_ = request.POST.get("From", "None    ")[:7]
        content = request.POST.get("Body", "No body.")
        print("New inbound with content {}".format(content))
        tags = Tag.extract_from(content)

        if not tags:
            response.message(settings.TAGS_MESSAGE)
        else:
            m = Musing.objects.create(areacode=from_, content=content)
            m.tags.set(tags)
            if m.assess():
                response.message(settings.SUCCESS_MESSAGE)
            else:
                response.message(settings.PENDING_MESSAGE)
        return HttpResponse(str(response))
