async function getQuestions() 
{
  let response = await fetch(questionsURL());
  let data = await response.json()
  return data;
}

function baseURL() {
    return "https://ca1951900c5e.ngrok.io/";
}

function questionsURL() {
    return baseURL() + "api/q/"
}

function tagsURL() {
    return baseURL() + "api/t/"
}

function questionURL(id) {
    return questionsURL() + id +"/"
}

async function getMusingForQuestion(question_id) {
  let response = await fetch(questionURL(question_id));
  let data = await response.json();
  return data;
}

window.addEventListener('load', (event) => {
    let word_time_factor = 800;
    let questions = [];
    let question_div = document.getElementById('musing-question-container');
    let musing_div = document.getElementById('musing-response-container');
    getQuestions().then(questions => {
        let i = 0;
        let words = 5;
        let timeout = words * word_time_factor;
        let classes = '';
        setTimeout(function next_musing(){
            getMusingForQuestion(questions[i].id).then(musing => {
                words = 0;
                classes = questions[i].tags.map(el => el.slice(1)).join(" ");

                question_div.className = classes;
                musing_div.className = classes;

                question_div.innerHTML = questions[i].content;

                if(musing) {
                    musing_div.innerHTML = musing.content;
                    words += musing.content.split(" ").length;
                } else {
                    musing_div.innerHTML = "no musings on this question yet!";
                }
                words += questions[i].content.split(" ").length;
                timeout = words * word_time_factor;
                i = (i + 1) % questions.length;
            }).catch(error => console.log(error))
            console.log("Setting timeout " + timeout + "ms");
            setTimeout(next_musing, timeout);
        });
    })
})
