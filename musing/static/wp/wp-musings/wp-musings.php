<?php
/**
* Plugin Name: Musings
* Plugin URI: https://musings.poplars.dev
* Description: Integrate musings into wordpress
* Version: 0.1
* Author: Poplar Development
* Author URI: https://poplars.dev
**/

function musing_client_load(){
    wp_enqueue_script(
        'musings-client', 
        'https://bbcdn.githack.com/cbinckly/musings/raw/master/musing/static/js/musing-client.js',
        array(),
        null
    );
}

add_action('wp_enqueue_scripts', 'musing_client_load');

function load_musing_question_block() {
  wp_enqueue_script(
    'musing-question-block',
    plugin_dir_url(__FILE__) . 'musing-question-block.js',
    array('wp-blocks','wp-editor'),
    true
  );
}

add_action('enqueue_block_editor_assets', 'load_musing_question_block');

function load_musing_response_block() {
  wp_enqueue_script(
    'musing-response-block',
    plugin_dir_url(__FILE__) . 'musing-response-block.js',
    array('wp-blocks','wp-editor'),
    true
  );
}

add_action('enqueue_block_editor_assets', 'load_musing_response_block');

?>
