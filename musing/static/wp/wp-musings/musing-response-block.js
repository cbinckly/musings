var el = wp.element.createElement;

wp.blocks.registerBlockType( 'musings/response-block', {
    title: 'Musing - Response',
    icon: 'dashicons-editor-quote',
    category: 'text',
    example: {},
    edit() {
        return el('div',
              {
                className: 'musing-response',
                id: 'musing-response-container'
              }, "The Musing response will appear here."
           );
    },
    save() {
        return el('div',
              {
                className: 'musing-response',
                id: 'musing-response-container'
              },
           );
    }
});

