var el = wp.element.createElement;

wp.blocks.registerBlockType( 'musings/question-block', {
    title: 'Musing - Question',
    icon: 'dashicons-align-center',
    category: 'text',
    example: {},
    edit() {
        return el('div',
              {
                className: 'musing-question',
                id: 'musing-question-container'
              }, "The Musing question will appear here."
           );
    },
    save() {
        return el('div',
              {
                className: 'musing-question',
                id: 'musing-question-container'
              },
           );
    }
});

