from rest_framework import serializers

from musing.models import Musing, Question, Tag

class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('tag', 'order', 'question', )

class MusingContentSerializer(serializers.ModelSerializer):
    tags = serializers.StringRelatedField(many=True, read_only=True)
    class Meta:
        model = Musing
        fields = ('content', 'tags')

class QuestionSerializer(serializers.ModelSerializer):
    tags = serializers.StringRelatedField(many=True, read_only=True)
    class Meta:
        model = Question
        fields = ('id', 'content', 'tags')
