from django.contrib import admin

from musing.models import Tag, Musing, Question

# Register your models here.
admin.site.register(Tag)
admin.site.register(Musing)
admin.site.register(Question)
